const checkTypeNumber  = (givenNumber) =>{
    // check data type
    if(givenNumber == undefined){
        return 'eror : Bro where is the parameter'
    }
    // check data type
    else if(typeof givenNumber !== 'number'){
        return "eror : INVALID data Type "
    }
    //check parameter genap / ganjil
    else {
        return givenNumber %2 == 0 ? 'GENAP' : 'GANJIL';
    }

}

console.log(checkTypeNumber(10))
console.log(checkTypeNumber(3))
console.log(checkTypeNumber("3"))
console.log(checkTypeNumber({}))
console.log(checkTypeNumber([]))
console.log(checkTypeNumber())