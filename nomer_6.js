function getAngkaTerbesarKeddua(arr){
    // check type data
    if (arr === undefined) {return 'eror : Bro where is the parameter'}
    if (!Array.isArray(arr)) {return "Error : invalid data type"}

    // menghilangkan nilai yang sama dalam array
    const arr1 = [...new Set (arr)];

// mengurutkan nilai dalam array dengan sort dan membalikan dari belakang sehingga diurutkan dari belakang dan mengambil nilai nomer 2 dalam array
    const second_highest = arr1.sort(function(a , b ){ return b - a ; })[1];
    return second_highest;
}
console.log(getAngkaTerbesarKeddua([9, 4, 7, 7, 3 ,2 ,2 ,8 ]));
console.log(getAngkaTerbesarKeddua(0));
console.log(getAngkaTerbesarKeddua());