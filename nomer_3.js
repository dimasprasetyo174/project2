function checkEmail (email) {
    // variabel untuk menampung regex ( @ dan .)
    const regex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    
    // check type data
    if (email === undefined){
        return 'eror : Bro where is the parameter'
    }
    // check type data
    if(typeof email !== 'string'){
        return 'invalid data input'
    }
    // check parameter dengan regex
    if (regex.test(email)){
        return 'valid'
    }
    else {
        return ' invalid'
    }
}
console.log(checkEmail('apranata@binar.co.id'))
console.log(checkEmail('apranata@binar.com'))
console.log(checkEmail('apranata@binar'))
console.log(checkEmail('apranata')) // TIDAK MEMENUHI kondisi karena tidak ada @ dan .
console.log(checkEmail(3332))
console.log(checkEmail())