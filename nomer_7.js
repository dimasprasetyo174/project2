const dataPenjualanPakAldi = [
    {
        namaProduct : 'spatu futsal nike academy 8',
        hargaSatuan : 760000,
        kategori : 'spatu sport',
        totalTerjual : 90,
    },
    {
        namaProduct : 'spatu futsal nike academy 8',
        hargaSatuan : 960000,
        kategori : 'spatu sport',
        totalTerjual : 37,
    },
    {
        namaProduct : 'spatu futsal nike academy 8',
        hargaSatuan : 360000,
        kategori : 'spatu sport',
        totalTerjual : 90,
    },
    {
        namaProduct : 'spatu futsal nike academy 8',
        hargaSatuan : 120000,
        kategori : 'spatu sport',
        totalTerjual : 90,
    }
]

const hitungTotalPenjualan = function (Object) {
    const total = dataPenjualanPakAldi[0].totalTerjual + dataPenjualanPakAldi[1].totalTerjual + dataPenjualanPakAldi[2].totalTerjual + dataPenjualanPakAldi[3].totalTerjual
    return total
}

console.log(hitungTotalPenjualan(dataPenjualanPakAldi))
