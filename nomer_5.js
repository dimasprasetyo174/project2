
function getSplitName (name) {
    // check type data
    if(typeof name !== 'string'){
        return "eror : INVALID data Type"
    }
    // variabel untuk menampung parameter yang di split
    const splitName = name.split(" ")

    // check lenght parameter
    if(splitName.length > 3){
        return 'This function in only for 3 character name';
    }
    else if(splitName.length === 3){
        return{
            firstName : splitName[0],
            secondName : splitName[1],
            thirdName : splitName[2]
            }
    }
    else if(splitName.length === 2){
        return{
            firstName : splitName[0],
            secondName : null,
            thirdName : splitName[1]
            }
    }
    else if(splitName.length === 1){
        return{
        firstName : splitName[0],
        secondName : null,
        thirdName : null
        }
    }   
}

console.log(getSplitName('Aldi Daniela Pranata'))
console.log(getSplitName('Dwi kuncoro'))
console.log(getSplitName('aurora'))
console.log(getSplitName('Aurora Aureliya Sukma Darma'))
console.log(getSplitName(0))
