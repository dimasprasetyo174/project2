function isValidPassword (password) {
    // variabel untuk mengecek parameter password
    const regex =  /^(?=.*\d)(?=.*[A-Z])(?=.*[a-z])(?=.*[a-zA-Z!#$@^%&? "])[a-zA-Z0-9!#$@^%&?]{8,}$/;
    
    // check type data
    if (password === undefined){
        return 'eror : Bro where is the parameter'
    }
    if(typeof password !== 'string'){
        return "eror : INVALID data Type"
    }
    // check parameter dengan regex
    if (regex.test(password)){
        return 'valid'
    }
    else {
        return ' invalid'
    }
}

console.log(isValidPassword('Meong2021'))
console.log(isValidPassword('meong2021'))
console.log(isValidPassword('@ong2021'))
console.log(isValidPassword('meong2'))
console.log(isValidPassword(0))
console.log(isValidPassword())